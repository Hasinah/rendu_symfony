<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use App\Entity\Chambre;
use App\Repository\HopitalRepository;

class ChambreFixtures extends Fixture implements DependentFixtureInterface
{
    private $hopitalRepository;

    public function __construct(HopitalRepository $hopitalRepository)
    {
        $this->hopitalRepository = $hopitalRepository;
    }


    public function load(ObjectManager $manager)
    {
        $hopitals = $this->hopitalRepository->findAll();


        foreach ($hopitals as $hopital){

            $chambreToCreate = rand(10,20);
            for($i = 1; $i <= $chambreToCreate; $i++){
                $chambre = new Chambre();
                $chambre->setHopital($hopital);
                $chambre->setNome("Chambre: $i");    
                $chambre->setCapacite(rand(1,3));
                $manager->persist($chambre);
            }
         
        }
       

        $manager->flush();
    }
    public function getDependencies()
    {
        return [
            HopitalFixtures::class
        ];
    }

}
