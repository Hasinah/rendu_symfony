<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Hopital;


class HopitalFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
   
        $names = [
            ["Grangette", "Chemin des Grangettes 7, 1224 Chêne-Bougeries"],
            ["Hug", "Chemin des Grangettes 7, 1224 Chêne-Bougeries"],
            ["Hopital de la Tour", "Chemin des Grangettes 7, 1224 Chêne-Bougeries"],
            ["Hopital Des enfants", "Chemin des Grangettes 7, 1224 Chêne-Bougeries"],
            ["Maternité", "Chemin des Grangettes 7, 1224 Chêne-Bougeries"],
        ];

        foreach ($names as $name){

            $hopital = new Hopital();
            $hopital->setNome($name[0]);
            $hopital->setAdresse($name[1]);
            $manager->persist($hopital);
        }
        $manager->flush();
    }
}
