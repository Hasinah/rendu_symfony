<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Patient;




class PatientFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $names = [
            ["Hasinah", "Tabet", "7.77", ],
            ["Aya", "Said", "756" ],
            ["Chauve", "Eli", "93" ],
            ["Tabet", "Hasinah", "756" ],
            ["Karoui", "Rania", "756.196" ],
            ["Taha", "Redein", "75.97" ],
            ["Tim", "Izzo", "78.98" ],
            ["Nabil", "Fabian", "75.99" ],
            ["Tomas", "Susane", "78.90" ],
            ["Afonso", "Liliana", "78.94" ],
        ];

        foreach ($names as $name){

            $patient = new Patient();
            $patient
                ->setNome($name[0])
                ->setPrenome($name[1])
                ->setAvs($name[2])
            ;

            $manager->persist($patient);

        }

        $manager->flush();
    }
}
