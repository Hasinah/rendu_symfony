<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PatientRepository")
 */
class Patient
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $nome;

    /**
     * @ORM\Column(type="string")
     */
    private $avs;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateNaissance;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $prenome;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Admission", mappedBy="patient")
     */
    private $admissions;

    public function __construct()
    {
        $this->admissions = new ArrayCollection();
    }
    public function __toString(){// liste deroulante
        return $this->getNome() ." ".$this->getPrenome();
    
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNome(): ?string
    {
        return $this->nome;
    }

    public function setNome(string $nome): self
    {
        $this->nome = $nome;

        return $this;
    }

    public function getAvs(): ?string
    {
        return $this->avs;
    }

    public function setAvs(string $avs): self
    {
        $this->avs = $avs;

        return $this;
    }

    public function getDateNaissance(): ?\DateTimeInterface
    {
        return $this->dateNaissance;
    }

    // public function setDateNaissance(\DateTimeInterface $dateNaissance): self
    // {
    //     $this->dateNaissance = $dateNaissance;

    //     return $this;
    // }

    public function setDateNaissance($dateNaissance): self
    {
        $this->dateNaissance = $dateNaissance;

        return $this;
    }

    public function getPrenome(): ?string
    {
        return $this->prenome;
    }

    public function setPrenome(?string $prenome): self
    {
        $this->prenome = $prenome;

        return $this;
    }

    /**
     * @return Collection|Admission[]
     */
    public function getAdmissions(): Collection
    {
        return $this->admissions;
    }

    public function addAdmission(Admission $admission): self
    {
        if (!$this->admissions->contains($admission)) {
            $this->admissions[] = $admission;
            $admission->setPatient($this);
        }

        return $this;
    }

    public function removeAdmission(Admission $admission): self
    {
        if ($this->admissions->contains($admission)) {
            $this->admissions->removeElement($admission);
            // set the owning side to null (unless already changed)
            if ($admission->getPatient() === $this) {
                $admission->setPatient(null);
            }
        }

        return $this;
    }
}
