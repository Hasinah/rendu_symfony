<?php

namespace App\Controller;
use App\Entity\Patient;
use App\Entity\Admission;
use App\Form\AdmissionType;
use App\Repository\AdmissionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @Route("/admission")
 */
class AdmissionController extends AbstractController
{
    /**
     * @Route("/", name="admission_index", methods={"GET"})
     */
    public function index(AdmissionRepository $admissionRepository): Response
    {
        return $this->render('admission/index.html.twig', [
            'admissions' => $admissionRepository->findAll(),
        ]);
    }
    
    /**
     * @Route("/{id}", name="admission_cloture", methods={"POST"})
     */
    public function cloture(Request $request, Admission $admission): Response
    {
        if ($this->isCsrfTokenValid('cloture'.$admission->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $admission->setDateFin(new \DateTime('now'));// la date de fin prend le valeur de moment actuelle
            $entityManager->flush();
        }

        return $this->redirectToRoute('admission_index');
    }
}
