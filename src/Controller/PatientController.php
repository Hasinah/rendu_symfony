<?php

namespace App\Controller;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Patient;
use App\Entity\Admission;
use App\Form\AdmissionType;
use App\Form\PatientType;
use App\Repository\AdmissionRepository;
use App\Repository\PatientRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/patient")
 */
class PatientController extends AbstractController
{
    /**
     * @Route("/", name="patient_index")
     */
    public function index(PatientRepository $patientRepository , Request $request): Response
    {
        $patient = new Patient();
        $form = $this->createForm(PatientType::class, $patient);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($patient);
            $entityManager->flush();

            return $this->redirectToRoute('patient_index');
        }
        return $this->render('patient/index.html.twig', [
            'patients' => $patientRepository->findAll(),
            'patient' => $patient,
            'form' => $form->createView(),
        ]);

    }



    /**
     * @Route("/new", name="patient_new", methods={"GET","POST"})
     */
    // public function new(Request $request): Response
    // {
    //     // $patient = new Patient();
    //     // $form = $this->createForm(PatientType::class, $patient);
    //     // $form->handleRequest($request);

    //     // if ($form->isSubmitted() && $form->isValid()) {
    //     //     $entityManager = $this->getDoctrine()->getManager();
    //     //     $entityManager->persist($patient);
    //     //     $entityManager->flush();

    //     //     return $this->redirectToRoute('patient_index');
    //     // }

    //     // return $this->render('patient/new.html.twig', [
    //     //     'patient' => $patient,
    //     //     'form' => $form->createView(),
    //     // ]);
    // }

    /**
     * @Route("/{id}", name="patient_show", methods={"GET","POST"})
     */
    public function show(Request $request, Patient $patient, EntityManagerInterface $manager, AdmissionRepository $admissionRepository): Response
    {
        $admissionToCreate = new Admission();
        $admissionToCreate->setPatient($patient);
        $admissionForm = $this->createForm(AdmissionType::class, $admissionToCreate);

        $admissionForm->handleRequest($request);
        if($admissionForm->isSubmitted() && $admissionForm->isValid()){
            $manager->persist($admissionToCreate);
            $manager->flush();

            return $this->redirectToRoute('patient_index', [
                'id' => $admissionToCreate->getId()
            ]);
        }
        
        $form = $this->createForm(PatientType::class, $patient);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('patient_index');
        }
        return $this->render('patient/edit.html.twig', [
            'patient' => $patient,
            'form' => $form->createView(),
            'createAdmissionForm' => $admissionForm->createView(),
            'admissions' => $admissionRepository->findAll(),
          
        ]);
       
    }

    
    /**
     * @Route("/{id}", name="patient_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Patient $patient): Response
    {
        if ($this->isCsrfTokenValid('delete'.$patient->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($patient);
            $entityManager->flush();
        }

        return $this->redirectToRoute('patient_index');
    }
}
